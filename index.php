<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Tools</title>

        <script src="assets/plugins/jquery/jquery.js"></script>

        <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css" />
        <script src="assets/plugins/bootstrap/js/bootstrap.js"></script>

        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.css" />

        <script src="assets/plugins/clipboard.js/clipboard.min.js"></script>

        <link rel="stylesheet" href="assets/css/style.css" />
        <style>
            body {
                padding-top: 60px;
                font-size: 14px;
            }
            html {
                position: relative;
                min-height: 100%;
            }
            body {
                margin-bottom: 60px;
            }
            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 60px;
                background-color: #f5f5f5;
            }
            .container .text-muted {
                margin: 20px 0;
            }
            .footer > .container {
                padding-right: 15px;
                padding-left: 15px;
            }
        </style>
        <script>
            $(function () {
                new Clipboard('.clipboard');
            });
        </script>
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./">Tools</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="?tool=orm">ORM</a></li>
                        <li><a href="?tool=controller">Controller</a></li>
                        <li><a href="?tool=service">Service</a></li>
                        <li><a href="?tool=dao">DAO</a></li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="?tool=dbconfig">dbconfig</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <?php
            $tool = filter_input(INPUT_GET, 'tool');
            if (!empty($tool)) {
                if ($tool != 'dbconfig') {
                    require 'src/pdo.php';
                }
                require_once "tools/$tool.php";
            } else {
                ?>
                <div class="jumbotron">
                    <h1>Tools!</h1>
                    <p class="lead">Generador de codigo y prototipo de proyecto.</p>
                    <p><a class="btn btn-lg btn-success" href="#" role="button"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Prototipo</a></p>
                </div>

                <?php
            }
            ?>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="text-muted">© 2017 Sernas & Cuevas. Todos los derechos reservados</p>
            </div>
        </footer>
    </body>
</html>
