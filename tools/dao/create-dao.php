<?php
$table = filter_input(INPUT_GET, 'table');
$namespace = filter_input(INPUT_GET, 'namespace');
$dao = filter_input(INPUT_GET, 'dao');

require 'src/functions/createdao.php';
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-terminal" aria-hidden="true"></i> DAO</h3>
            </div>
            <div class="panel-body">
                <div class="row m-b-5">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary btn-sm clipboard" data-clipboard-target="#code">Copiar</button>
                    </div>
                </div>
                <pre id="code"><?= createdao($pdo, $namespace, $dao, $table); ?></pre>
            </div>
        </div>
    </div>
</div>