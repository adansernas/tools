<?php
require 'src/functions/listtables.php';
$tablas = listtables($dbname, $pdo);

$options = '<option value="">Seleccione una opcion</option>';
foreach ($tablas as $tabla) {
    $name = $tabla['name'];
    $exp = explode('_', $name);

    $schema = $exp[0];
    $tbname = $exp[1];
    $options .= '<option value="' . $name . '" data-namespace="' . $schema . '" data-pojo="' . $tbname . '" >' . $name . '</option>';
}
?> 
<script>
    $(function () {
        $("#table").on("change", function () {
            try {
                var $option = $("#table").find("option:selected");
                $("#namespace").val($option.attr("data-namespace"));
                $("#dao").val($option.attr("data-pojo"));
            } catch (er) {
                console.log(er);
            }
        });
    });
</script>
<div class="row m-t-15">
    <div class="col-md-12">
        <form class="form-horizontal" method="GET" >
            <input type="hidden" name="tool" value="dao" />
            <input type="hidden" name="action" value="create-dao" />

            <div class="form-group">
                <label class="col-md-2 control-label" for="table">Tabla</label>
                <div class="col-md-4">
                    <select id="table" name="table" class="form-control"><?= $options ?></select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" >namespace</label>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">namespace dao\</span>
                        <input id="namespace" name="namespace" class="form-control" placeholder="" type="text">
                    </div>

                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="dao">Dao</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <input id="dao" name="dao" class="form-control" placeholder="Clase" type="text">
                        <span class="input-group-addon">Dao</span>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-success">Crear</button>
                    <button type="reset" class="btn btn-danger">Limpiar</button>
                </div>
            </div>

        </form>
    </div>
</div>