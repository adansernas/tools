<?php

$action = filter_input(INPUT_GET, 'action');
if (empty($action)) {
    include "$tool/form.php";
} else {
    include "$tool/$action.php";
}