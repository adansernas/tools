<?php require 'src/functions/summary.php';
$summary = summary($dbname, $pdo, $table);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-terminal" aria-hidden="true"></i> summary</h3>
            </div>
            <div class="panel-body">
                <form class="form-horizontal">
                    <fieldset>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Rows</label>
                            <div class="col-md-2">
                                <input class="form-control text-right" type="text" readonly="" value="<?= $summary['rows'] ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-1 control-label">Size</label>
                            <div class="col-md-2">
                                <div class="input-group">
                                    <input class="form-control text-right" type="text" readonly="" value="<?= $summary['kb'] ?>">
                                    <span class="input-group-addon">KB</span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>

            </div>
        </div>
    </div>
</div>