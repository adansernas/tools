<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-terminal" aria-hidden="true"></i> describe</h3>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Field</th>
                            <th>Type</th>
                            <th>Extra</th>
                            <th>Key</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sth = $pdo->prepare('DESCRIBE ' . $table);
                        $sth->execute();
                        $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

                        foreach ($campos as $campo) {
                            echo "<tr><td>{$campo['Field']}</td><td>{$campo['Type']}</td><td>{$campo['Extra']}</td><td>{$campo['Key']}</td></tr>";
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>