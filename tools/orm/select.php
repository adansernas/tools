<?php require 'src/functions/createselect.php'; ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-terminal" aria-hidden="true"></i> SELECT</h3>
            </div>
            <div class="panel-body">
                <div class="row m-b-5">
                    <div class="col-md-12 text-right">
                        <button class="btn btn-primary btn-sm clipboard" data-clipboard-target="#code">Copiar</button>
                    </div>
                </div>
                <pre id="code"><?= createselect($pdo, $table); ?></pre>
            </div>
        </div>
    </div>
</div>