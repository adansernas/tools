<div class="row m-b-15">
    <div class="col-md-12">
        <ul>
            <?php
            require 'src/functions/listtables.php';
            $tablas = listtables($dbname, $pdo);
            foreach ($tablas as $tabla) {
                echo "<li><a href='?tool=orm&action=summary&table={$tabla['name']}'>{$tabla['name']}</a></li>";
            }
            ?>
        </ul>
    </div>
</div>