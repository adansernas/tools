<div class="row m-t-20">
    <div class="col-md-12">
        <form class="form-horizontal" method="GET" >
            <input type="hidden" name="tool" value="dbconfig" />
            <input type="hidden" name="action" value="save-dbconfig" />

            <div class="form-group">
                <label class="col-md-2 control-label" for="inputHost">Host</label>  
                <div class="col-md-2">
                    <input id="inputHost" name="host" type="text" placeholder="Host" class="form-control input-md">
                </div>
                <label class="col-md-1 control-label" for="inputPort">Port</label>  
                <div class="col-md-2">
                    <input id="inputPort" name="port" type="text" placeholder="Port" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="inputUsername">Username</label>  
                <div class="col-md-3">
                    <input id="inputUsername" name="username" type="text" placeholder="Username" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="inputPassword">Password</label>  
                <div class="col-md-3">
                    <input id="inputPassword" name="password" type="text" placeholder="Password" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label" for="inputDatabase">Database</label>  
                <div class="col-md-4">
                    <input id="inputDatabase" name="database" type="text" placeholder="Database" class="form-control input-md">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-2">
                    <button type="submit" class="btn btn-success">Guardar Configuracion</button>
                    <button type="reset" class="btn btn-danger">Limpiar</button>
                </div>
            </div>

        </form>

    </div>
</div>

