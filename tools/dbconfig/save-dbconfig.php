<?php

try {
    $host = filter_input(INPUT_GET, 'host');
    $port = filter_input(INPUT_GET, 'port');
    $user = filter_input(INPUT_GET, 'username');
    $pswd = filter_input(INPUT_GET, 'password');
    $dbname = filter_input(INPUT_GET, 'database');

    $connectionString = "mysql:host=$host;port=$port;dbname=$dbname";
    $pdo = new PDO($connectionString, $user, $pswd);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if (is_readable('src/dbconfig.ini')) {
        unlink('src/dbconfig.ini');
    }
    $stream = fopen('src/dbconfig.ini', 'w');
    $ln = "[database]\n";
    fwrite($stream, $ln);
    
    $ln = "driver = \"mysql\"\nhost = \"$host\"\nport = \"$port\"\n";
    fwrite($stream, $ln);
    $ln = "dbname = \"$dbname\"\nuser = \"$user\"\npswd = \"$pswd\"\n";
    fwrite($stream, $ln);
    
    fclose($stream);

    echo '<div class="alert alert-success" role="alert"><strong>Conexión exitosa!</strong> Se logro establecer la conexión</div>';
} catch (PDOException $ex) {
    echo '<div class="alert alert-danger" role="alert"><strong>No se pudo establecer la conexión!</strong> ' . utf8_encode($ex->getMessage()) . '</div>';
}