<?php
$action = filter_input(INPUT_GET, 'action');
$table = filter_input(INPUT_GET, 'table');
if (!empty($action) && !empty($table)) {
    ?>
    <h4><?= $table ?></h4>
    <div class="row m-b-15">
        <div class="col-md-12">
            <div class="btn-group btn-group-justified" role="group" aria-label="...">
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=summary&table=<?= $table ?>">summary</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=data&table=<?= $table ?>">data</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=describe&table=<?= $table ?>">describe</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=select&table=<?= $table ?>">select</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=insert&table=<?= $table ?>">insert</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=update&table=<?= $table ?>">update</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=pojo&table=<?= $table ?>">pojo</a>
                </div>
                <div class="btn-group" role="group">
                    <a class="btn btn-default" href="?tool=orm&action=enum&table=<?= $table ?>">enum</a>
                </div>
            </div>
        </div>
    </div>
    <?php
    include "$tool/$action.php";
} else {
    include "$tool/list-tables.php";
}
