<?php

try {
    $configuration = parse_ini_file('dbconfig.ini', true);
    $database = $configuration['database'];
    $driver = $database['driver'];
    $host = $database['host'];
    $port = $database['port'];
    $dbname = $database['dbname'];
    $user = $database['user'];
    $pswd = $database['pswd'];

    $connectionString = "$driver:host=$host;port=$port;dbname=$dbname";
    $pdo = new PDO($connectionString, $user, $pswd);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET CHARACTER SET utf8');
} catch (Exception $ex) {
    echo '<div class="alert alert-danger" role="alert"><strong>No se pudo establecer la conexión!</strong> ' . utf8_encode($ex->getMessage()) . '</div>';
    die();
}