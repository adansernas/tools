<?php

function createenum($pdo, $table) {
    $sth = $pdo->prepare('DESCRIBE ' . $table);
    $sth->execute();
    $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

    $sth = $pdo->prepare('SELECT * FROM ' . $table);
    $sth->execute();
    $registros = $sth->fetchAll(PDO::FETCH_ASSOC);

    foreach ($campos as $campo) {
        if ($campo['Key'] === 'PRI') {
            $fieldValue = $campo['Field'];
        }
    }

    $exp = explode('_', $table);

    $schema = $exp[0];
    $tbname = $exp[1];
    $fieldLabel = lcfirst($tbname);

    $fechaActual = date("d-m-Y H:i:s");

    $clss = '';
    $clss .= ( "namespace enumeracion\\$schema;\n\n");

    $clss .= ( "/**\n * Valores de la tabla: $table\n * Fecha de generación: $fechaActual\n */\n");
    $clss .= ( "class Enum$tbname {\n\n");

    foreach ($registros as $registro) {
        $label = str_replace(' ', '_', $registro[$fieldLabel]);
        $clss .= ( "\tconst {$label} = {$registro[$fieldValue]};\n\n");
    }

    $clss .= ( "\t//Constructor\n");
    $clss .= ( "\tfunction __construct() {\n\n\t}\n\n");

    // Fin de la clase
    $clss .= ( "}\n");
    
    return $clss;
}
