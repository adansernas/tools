<?php

function createupdate($pdo, $table) {
    $sth = $pdo->prepare('DESCRIBE ' . $table);
    $sth->execute();
    $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

    $exp = explode('_', $table);

    $pojo = $exp[1];
    $pojo = lcfirst($pojo);
    
    $clss = '';
    $clss .= 'public function actualizar' . ucfirst($pojo) . '(' . ucfirst($pojo) . ' $' . $pojo . '): bool {';
    $clss .= "\n";
    $clss .= 'try {';
    $clss .= "\n";
    $clss .= '$sth = $this->getPdo()->prepare(' . "'UPDATE $table SET '\n";


    $pk = '';
    $it = 0;
    $mxi = count($campos) - 1;
    foreach ($campos as $campo) {
        if ($campo['Key'] === 'PRI') {
            $pk = $campo['Field'];
        } else {
            $coma = ($it == $mxi) ? '' : ',';
            $clss .= ". '{$campo['Field']} = :{$campo['Field']}$coma ' \n";
        }
        $it++;
    }

    $clss .= "\n. 'WHERE $pk = :$pk');";
    $clss .= "\n";
    foreach ($campos as $campo) {
        $clss .= '$sth->bindValue(' . "':{$campo['Field']}', " . '$' . $pojo . '->get' . ucfirst($campo['Field']) . '(), ';
        if (strpos($campo['Type'], 'int') > -1) {
            $clss .= 'PDO::PARAM_INT';
        } elseif (strpos($campo['Type'], 'varchar') > -1) {
            $clss .= 'PDO::PARAM_STR';
        } elseif (strpos($campo['Type'], 'datetime') > -1) {
            $clss .= 'PDO::PARAM_STR';
        } elseif (strpos($campo['Type'], 'date') > -1) {
            $clss .= 'PDO::PARAM_STR';
        } elseif (strpos($campo['Type'], 'decimal') > -1) {
            $clss .= 'PDO::PARAM_STR';
        } elseif (strpos($campo['Type'], 'text') > -1) {
            $clss .= 'PDO::PARAM_STR';
        }
        $clss .= ');';
        $clss .= "\n";
    }
    $clss .= "\n";
    $clss .= '$sth->execute();';
    $clss .= "\n";
    $clss .= "\n";
    $clss .= 'return true;';
    $clss .= "\n";
    $clss .= '} catch (Exception $ex) {';
    $clss .= "\n\t";
    $clss .= 'throw new UpdateException($ex);';
    $clss .= "\n";
    $clss .= '}';
    $clss .= "\n";
    $clss .= '}';

    return $clss;
}
