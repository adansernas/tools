<?php

function summary($dbname, $pdo, $table) {
    $sth = $pdo->prepare('SELECT TABLE_NAME AS "name", '
            . 'table_rows AS "rows", '
            . 'ROUND((data_length + index_length) / 1024, 2 ) AS "kb" '
            . 'FROM information_schema.TABLES '
            . 'WHERE table_schema = "' . $dbname . '" '
            . 'AND TABLE_NAME = "' . $table . '"');
    $sth->execute();
    $registro = $sth->fetch(PDO::FETCH_ASSOC);
    return $registro;
}
