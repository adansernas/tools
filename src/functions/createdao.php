<?php

include 'createinsert.php';
include 'createupdate.php';
include 'createselectrs.php';
include 'createselect.php';

function createdao($pdo, $namespace, $dao, $table) {
    $clss = '';
    $clss .= "namespace dao\\" . $namespace . ";\n";
    $clss .= "\n";
    $clss .= "use PDO;\n";
    $clss .= "use Exception;\n";
    $clss .= "use exceptions\SelectException;\n";
    $clss .= "use exceptions\InsertException;\n";
    $clss .= "use exceptions\UpdateException;\n";
    $clss .= "use clases\utils\Dao;\n";
    $clss .= "use clases\orm\ResultSet;\n";

    if (!empty($table)) {
        $exp = explode('_', $table);

        $schema = $exp[0];
        $tbname = $exp[1];
        $clss .= "use models\\$schema\\$tbname;\n";
    }

    $clss .= "\n";
    $clss .= "/**\n";
    $clss .= "* Description of {$dao}Dao\n";
    $clss .= "*\n";
    $clss .= "*/\n";
    $clss .= "class {$dao}Dao extends Dao {\n";
    if (!empty($table)) {
        $clss .= "\n";
        $insert = createselectrs($pdo, $table);
        $clss .= $insert;
        $clss .= "\n";
        $clss .= "\n";
        $insert = createselect($pdo, $table);
        $clss .= $insert;
        $clss .= "\n";
        $clss .= "\n";
        $insert = createinsert($pdo, $table);
        $clss .= $insert;
        $clss .= "\n";
        $clss .= "\n";
        $update = createupdate($pdo, $table);
        $clss .= $update;
        $clss .= "\n";
    }
    $clss .= "\n";
    $clss .= "}\n";
    return $clss;
}
