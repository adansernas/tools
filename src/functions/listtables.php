<?php

function listtables($dbname, $pdo) {
    $sth = $pdo->prepare('SELECT TABLE_NAME AS name FROM information_schema.TABLES WHERE table_schema = "' . $dbname . '"');
    $sth->execute();
    $registros = $sth->fetchAll(PDO::FETCH_ASSOC);
    return $registros;
}
