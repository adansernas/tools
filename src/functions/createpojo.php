<?php

function createpojo($pdo, $table) {
    $sth = $pdo->prepare('DESCRIBE ' . $table);
    $sth->execute();
    $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

    $exp = explode('_', $table);

    $schema = $exp[0];
    $tbname = $exp[1];

    $fechaActual = date("d-m-Y H:i:s");

    $clss = '';
    $clss .= "namespace models\\$schema;\n\n";

    $clss .= "/**\n * Entidad para la tabla: $table\n * Fecha de generación: $fechaActual\n */\n";
    $clss .= "class $tbname {\n\n";

    $clss .= "\t//Atributos\n";
    foreach ($campos as $campo) {
        $nmfield = $campo['Field'];

        $clss .= "\tprivate $$nmfield;\n";
    }

    $clss .= "\t//Constructor\n";
    $clss .= "\tfunction __construct() {\n\n\t}\n\n";

    // Getters & Setters
    foreach ($campos as $campo) {
        $nmfield = $campo['Field'];

        $nmget = ucfirst($nmfield);

        // getter
        $clss .= "\tpublic function get$nmget(){\n\t";
        $clss .= 'return $this->' . "$nmfield;\n\n";
        $clss .= "\t}\n\n";
        // setter
        $clss .= "\tpublic function set$nmget($$nmfield){\n\t";
        $clss .= '$this->' . "$nmfield = $$nmfield;\n\n";
        $clss .= "\t}\n\n";
    }


    // Fin de la clase
    $clss .= "}\n";

    return $clss;
}
