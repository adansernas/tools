<?php

function createselect($pdo, $table) {

    $sth = $pdo->prepare('DESCRIBE ' . $table);
    $sth->execute();
    $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

    $exp = explode('_', $table);

    $pojo = $exp[1];
    $pojo = lcfirst($pojo);

    $method = '';
    $method .= 'public function obtener' . ucfirst($pojo) . '(int $id' . ucfirst($pojo) . '): array {';
    $method .= "\n";
    $method .= 'try {';
    $method .= "\n";
    $method .= '$sth = $this->getPdo()->prepare(' . "'SELECT * '";
    $method .= "\n";
    $method .= ". 'FROM $table AS v '";
    $method .= "\n";
    $method .= ". 'WHERE v.id" . ucfirst($pojo) . " = :id" . ucfirst($pojo) . ";');";
    $method .= "\n";
    $method .= '$sth->execute(array' . "(':id" . ucfirst($pojo) . "'" . '=> $id' . ucfirst($pojo) . "));";
    $method .= "\n";
    $method .= '$' . $pojo . ' = $sth->fetch(PDO::FETCH_ASSOC);';
    $method .= "\n";
    $method .= "\n";
    $method .= 'return $' . $pojo . ';';
    $method .= "\n";
    $method .= '} catch (Exception $ex) {';
    $method .= "\n\t";
    $method .= 'throw new SelectException($ex);';
    $method .= "\n";
    $method .= '}';
    $method .= "\n";
    $method .= '}';

    return $method;
}
