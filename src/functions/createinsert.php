<?php

function createinsert($pdo, $table) {
    $sth = $pdo->prepare('DESCRIBE ' . $table);
    $sth->execute();
    $campos = $sth->fetchAll(PDO::FETCH_ASSOC);

    $exp = explode('_', $table);

    $pojo = $exp[1];
    $pojo = lcfirst($pojo);

    $clss = '';

    $clss .= 'public function guardar' . ucfirst($pojo) . '(' . ucfirst($pojo) . ' $' . $pojo . '): int {';
    $clss .= "\n";
    $clss .= 'try {';
    $clss .= "\n";
    $clss .= '$sth = $this->getPdo()->prepare(';
    $clss .= "'INSERT INTO $table(";

    $pk = '';
    $inserts = array();
    foreach ($campos as $campo) {
        if ($campo['Key'] === 'PRI') {
            $pk = $campo['Field'];
        } else {
            $inserts[] = $campo['Field'];
        }
    }


    $clss .= implode(', ', $inserts);
    $clss .= ")'\n . ' VALUES(:";
    $clss .= implode(', :', $inserts);
    $clss .= ");');";
    $clss .= "\n";

    foreach ($campos as $campo) {
        if ($campo['Key'] === 'PRI') {
            
        } else {
            $clss .= '$sth->bindValue(' . "':{$campo['Field']}', " . '$' . $pojo . '->get' . ucfirst($campo['Field']) . '(), ';
            if (strpos($campo['Type'], 'int') > -1) {
                $clss .= 'PDO::PARAM_INT';
            } elseif (strpos($campo['Type'], 'varchar') > -1) {
                $clss .= 'PDO::PARAM_STR';
            } elseif (strpos($campo['Type'], 'datetime') > -1) {
                $clss .= 'PDO::PARAM_STR';
            } elseif (strpos($campo['Type'], 'date') > -1) {
                $clss .= 'PDO::PARAM_STR';
            } elseif (strpos($campo['Type'], 'decimal') > -1) {
                $clss .= 'PDO::PARAM_STR';
            } elseif (strpos($campo['Type'], 'text') > -1) {
                $clss .= 'PDO::PARAM_STR';
            }
            $clss .= ');';
        }
        $clss .= "\n";
    }

    $clss .= "\n";
    $clss .= '$sth->execute();';
    $clss .= "\n";
    $clss .= "\n";
    $clss .= '$' . $pk . ' = $this->getPdo()->lastInsertId();';
    $clss .= "\n";
    $clss .= "\n";
    $clss .= 'return $' . $pk . ';';
    $clss .= "\n";
    $clss .= '} catch (Exception $ex) {';
    $clss .= "\n\t";
    $clss .= 'throw new InsertException($ex);';
    $clss .= "\n";
    $clss .= '}';
    $clss .= "\n";
    $clss .= '}';

    return $clss;
}
